
Build 
==

  ./do_build_sk.sh threadtest.c threadtest_39dev ../geos_39dev_build/


  ./do_build_sk.sh threadtest.c threadtest_38 ../geos_38_build


  ./do_build_sk.sh threadtest.c threadtest_37 ../geos-debian-3.7.1-1

Execute 
==

    schatzkammerlin$ for n in threadtest_3*;do 
      echo "exec: "$n;  
        time for xx in 1 2 3 4 5 6 7 8 9 A; do ./$n test.wkt >/dev/null;
      done;
    done


    exec: threadtest_37_3.7.0
    ..++..++..++..++..++..++..++..++..++..++
    real	0m5.390s
    user	0m9.148s
    sys	0m0.656s
  
    exec: threadtest_38_3.8.1
    ..++..++..++..++..++..++..++..++..++..++
    real	0m3.777s
    user	0m6.316s
    sys	0m0.456s
    exec: threadtest_39dev_3.9.0
    ..++..++..++..++..++..++..++..++..++..++
    real	0m3.669s
    user	0m5.996s
    sys	0m0.548s

---

