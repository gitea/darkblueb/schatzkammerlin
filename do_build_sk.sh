#!/bin/bash

# Number of make jobs to use:
JOBS=8

# Define CPU family to optimize for:
myarch="sandybridge"

# Optimization flags:
CFLAGS_OPT="-O2 -march=${myarch} -mtune=${myarch}"

# Debug flags:
CFLAGS_DEBUG="-v -ggdb3"

# C/C++ flags common to all builds:
CFLAGS_COMMON="${CFLAGS_DEBUG} ${CFLAGS_OPT} -fPIC"
CXXFLAGS_COMMON="${CFLAGS_COMMON}"

# Flags for compiling against libgeos
CXXFLAGS_GEOS="-std=c++11"

# GCC version for locating headers:
mygccver="8"

# GCC tuple for locating headers:
mytuple="x86_64-linux-gnu"

# These don't get included in the CXXFLAGS_COMMON so that they can follow project includes
CXXFLAGS_STDCXX="-I/usr/include/c++/${mygccver} -I/usr/include/${mytuple}/c++/${mygccver}"

# These go at the end of the build line.
CFLAGS_LINK_COMMON="-lm -lpthread  -lgeos_c"
CXXFLAGS_LINK_COMMON="${CFLAGS_LINK_COMMON} -lstdc++"


if [ $# -lt 3 ] ; then
	printf -- '\nUsage: %s <srcfile> <outfile_basename> </path/to/geos/src/base>...\n' "${0}"
	exit 0
fi

# Capture our source/out file info
mysrcfile="${1}"
myoutroot="${2}"
shift 2

# Loop over list of source dirs and build our target for each
while [ $# -gt 0 ] ; do
	# Grab the next dir and shift
	mydir="${1}" ; shift

	# Define our paths for this source dir
	GEOS_SRC_BASE="${mydir}"
	GEOS_LIB_PATH="${mydir}/build/lib"

	# If our lib hasn't been built yet, build it
        if ! [ -d "${GEOS_SRC_BASE}" ] ; then
            echo ${GEOS_SRC_BASE}" does not exist?"
            continue
        fi

	if ! [ -d "${GEOS_LIB_PATH}" ] ; then
		OLDIR="${PWD}"
		mkdir -p "${GEOS_SRC_BASE}/build" || break
		cd "${GEOS_SRC_BASE}/build"
		cmake -E CFLAGS="${CFLAGS_COMMON}" CXXFLAGS="${CXXFLAGS_COMMON}" cmake ..
		make -j ${JOBS}
		cd "${OLDDIR}"
	fi

	# Determine name of lib in this source dir
	GEOS_LIB="$(echo "${GEOS_LIB_PATH}"/libgeos.so.?.?.* )"

	# If we didn't find it, complain and skip
	if [ "${GEOS_LIB#${GEOS_LIB_PATH}/libgeos.so.}" == '?.?.*' ] ; then
		printf -- '\nCould not find "libgeos.so.?.?.*" in "%s", skipping!' "${GEOS_LIB_PATH}"
		continue
	fi

	# Build our cxx build/link flags for the current source tree.
	CXXFLAGS="${CXXFLAGS_COMMON} ${CXXFLAGS_GEOS} -I${GEOS_SRC_BASE}/include ${CXXFLAGS_STDCXX} -L${GEOS_LIB_PATH}"
	CXXFLAGS_LINK="-Wl,-rpath=${GEOS_LIB_PATH} ${CXXFLAGS_LINK_COMMON} -l:${GEOS_LIB##*/}"

	# Build our command line
	mycmd="gcc ${CXXFLAGS} ${mysrcfile} -o ${myoutroot}_${GEOS_LIB##*/libgeos.so.} ${CXXFLAGS_LINK}"

	# Print and run
	printf -- '\nRunning: %s\n' "${mycmd}"
	${mycmd}
done
